package main

import (
	"go_project/data"
	"go_project/handler"
	"go_project/middleware"
	"go_project/service"
	"log"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

func main() {
	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatal("Failed to initialize logger:", err)
	}
	defer logger.Sync()

	router := gin.Default()
	router.Use(middleware.ErrorHandler())
	err = data.Connect("postgres://hizikiel:hizikiel@localhost:5432/dbname?sslmode=disable", logger)
	if err != nil {
		logger.Fatal("Failed to connect to database:", zap.Error(err))
	}
	userRepo := data.NewUserRepository()
	userService := service.NewUserService(userRepo)
	userHandler := handler.NewUserHandler(userService, logger)
	router.POST("/register", userHandler.RegisterUser)
	router.Run(":8080")
}
