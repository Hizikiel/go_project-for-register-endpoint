package middleware

import (
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"net/http"
)

func ErrorHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Next()
		logger, err := zap.NewDevelopment()
		if err != nil {
			panic(err)
		}
		defer logger.Sync()

		errors := c.Errors
		var statusCode int
		if len(errors) > 0 {
			for _, e := range errors {
				statusCode = http.StatusInternalServerError
				c.AbortWithStatusJSON(statusCode, gin.H{"error": e.Error()})
				logger.Error("error", zap.Error(e))
			}
		}
	}
}
