package middleware

import (
	"context"
	"net/http"
	"time"

	"log"

	"github.com/gin-gonic/gin"
)

func Timeout(d time.Duration) gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(c.Request.Context(), d)
		defer cancel()

		c.Request = c.Request.WithContext(ctx)
		c.Next()

		if ctx.Err() == context.DeadlineExceeded {
			log.Println("Request timed out")
			c.AbortWithStatusJSON(http.StatusRequestTimeout, gin.H{"error": "request timed out"})
		}
	}
}
