package handler

import (
	"context"
	"github.com/gin-gonic/gin"
	"github.com/joomcode/errorx"
	"go_project/middleware"
	"go_project/service"
	"go.uber.org/zap"
	"net/http"
	"time"
)

var (
	UserErrors            = errorx.NewNamespace("user_errors")
	ErrInvalidRequest     = UserErrors.NewType("invalid_request")
	ErrRegistrationFailed = UserErrors.NewType("registration_failed")
)

type UserHandler struct {
	userService service.UserService
	logger      *zap.Logger
}

func NewUserHandler(userService service.UserService, logger *zap.Logger) *UserHandler {
	return &UserHandler{userService: userService, logger: logger}
}

func (h *UserHandler) RegisterUser(c *gin.Context) {
	ctx, cancel := context.WithTimeout(c.Request.Context(), 10*time.Second)
	defer cancel()

	var req middleware.RegisterUserRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		wrappedErr := ErrInvalidRequest.Wrap(err, "failed to bind JSON")
		h.logger.Info("failed to bind JSON",
			zap.Error(wrappedErr),
			zap.String("url", c.Request.URL.String()),
			zap.String("method", c.Request.Method),
		)
		c.JSON(http.StatusBadRequest, gin.H{"error": wrappedErr.Error()})
		return
	}

	if err := h.userService.RegisterUser(ctx, req); err != nil {
		wrappedErr := ErrRegistrationFailed.Wrap(err, "user registration failed")
		h.logger.Info("user registration failed",
			zap.Error(wrappedErr),
			zap.String("url", c.Request.URL.String()),
			zap.String("method", c.Request.Method),
		)
		c.JSON(http.StatusInternalServerError, gin.H{"error": wrappedErr.Error()})
		return
	}

	h.logger.Info("User registered successfully",
		zap.String("email", req.Email),
		zap.String("url", c.Request.URL.String()),
		zap.String("method", c.Request.Method),
	)
	c.JSON(http.StatusOK, gin.H{"message": "User registered successfully"})
}
