package service

import (
	"context"
	"go_project/data"
	"go_project/middleware"

	"golang.org/x/crypto/bcrypt"
)

type UserService interface {
	RegisterUser(ctx context.Context, req middleware.RegisterUserRequest) error
}

type userService struct {
	userRepository data.UserRepository
	// emailService   EmailService
}

func NewUserService(userRepo data.UserRepository) UserService {
	return &userService{
		userRepository: userRepo,
	}
}

func (s *userService) RegisterUser(ctx context.Context, req middleware.RegisterUserRequest) error {

	hashedPassword, err := hashPassword(req.Password)
	if err != nil {
		return err
	}
	user := data.User{
		Username: req.Username,
		Email:    req.Email,
		Password: hashedPassword,
	}

	return s.userRepository.CreateUser(ctx, user)
}
func hashPassword(password string) (string, error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	return string(hashedPassword), nil
}
