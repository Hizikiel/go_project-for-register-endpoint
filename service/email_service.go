package service

type EmailService interface {
	SendWelcomeEmail(email string) error
}

type emailService struct{}

func NewEmailService() EmailService {
	return &emailService{}
}

func (s *emailService) SendWelcomeEmail(email string) error {
	return nil
}
