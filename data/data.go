package data

import (
	"database/sql"
	"github.com/joomcode/errorx"
	_ "github.com/lib/pq"
	"go.uber.org/zap"
)

var (
	DB              *sql.DB
	DatabaseErrors  = errorx.NewNamespace("database_errors")
	ErrOpenDatabase = DatabaseErrors.NewType("open_database_failed")
	ErrPingDatabase = DatabaseErrors.NewType("ping_database_failed")
)

func Connect(dataSourceName string, logger *zap.Logger) error {
	var err error
	DB, err = sql.Open("postgres", dataSourceName)
	if err != nil {
		wrappedErr := ErrOpenDatabase.Wrap(err, "error opening database")
		logger.Error("Error opening database",
			zap.Error(wrappedErr),
			zap.String("dataSourceName", dataSourceName),
		)
		return wrappedErr
	}

	if err = DB.Ping(); err != nil {
		wrappedErr := ErrPingDatabase.Wrap(err, "error pinging database")
		logger.Error("Error pinging database",
			zap.Error(wrappedErr),
			zap.String("dataSourceName", dataSourceName),
		)
		return wrappedErr
	}

	logger.Info("Successfully connected to the database", zap.String("dataSourceName", dataSourceName))
	return nil
}
