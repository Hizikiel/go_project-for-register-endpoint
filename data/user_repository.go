package data

import (
	"context"
	"errors"
)

type User struct {
	Username string
	Email    string
	Password string
}

type UserRepository interface {
	CreateUser(ctx context.Context, user User) error
}

type userRepository struct {
	users map[string]User
}

func NewUserRepository() UserRepository {
	return &userRepository{users: make(map[string]User)}
}

func (r *userRepository) CreateUser(ctx context.Context, user User) error {
	if _, exists := r.users[user.Username]; exists {
		return errors.New("user already exists")
	}
	r.users[user.Username] = user
	return nil
}
